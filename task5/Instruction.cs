﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace task5
{
    internal class Instruction : AbstractDoc
    {
        public Instruction()
        {
            title = "Very cool instruction title";
            body = "The most interestin instruction body";
            footer = "An amazing instruction footer";
        }
        public override string Title
        {
            get
            {
                if (title != null)
                    return title;
                else
                    return "Instruction does not have any title";
            }
        }
        public override string Body
        {
            get
            {
                if (body != null)
                    return body;
                else
                    return "Instruction does not have text";
            }
        }
        public override string Footer
        {
            get
            {
                if (footer != null)
                    return footer;
                else
                    return "Footer is empty";
            }
        }

        public override void Show()
        {
            Console.WriteLine($"Instruction title: {title}");
            Console.WriteLine($"Instruction content: {body}");
            Console.WriteLine($"Instruction footer: {footer}");
        }
    }
}
