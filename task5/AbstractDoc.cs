﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5
{
    abstract class AbstractDoc
    {
        protected string title = string.Empty;
        protected string body = string.Empty;
        protected string footer = string.Empty;

        public abstract string Title { get; }
        public abstract string Body { get; }
        public abstract string Footer { get; }
        public abstract void Show();
    }
}
