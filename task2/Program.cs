﻿namespace task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            XMLHandler xmlhandler = new XMLHandler();
            xmlhandler.Open();
            xmlhandler.Create();
            xmlhandler.Change();
            xmlhandler.Save();
            Console.WriteLine("--------------------------------------");

            TXTHandler txthandler = new TXTHandler();
            txthandler.Open();
            txthandler.Create();
            txthandler.Change();
            txthandler.Save();
            Console.WriteLine("--------------------------------------");

            DOCHandler dochandler = new DOCHandler();
            dochandler.Open();
            dochandler.Create();
            dochandler.Change();
            dochandler.Save();
            Console.WriteLine("--------------------------------------");
        }
    }
}