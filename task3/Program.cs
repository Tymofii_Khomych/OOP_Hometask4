﻿namespace task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Player player = new Player();

            IPlayable playerGaming = player as IPlayable;
            playerGaming.Play();
            playerGaming.Pause();
            playerGaming.Stop();

            Console.WriteLine("-----------------------------------------------");

            IRecordable playerRecording = player as IRecordable;
            playerRecording.Record();
            playerRecording.Pause();
            playerRecording.Stop();

        }
    }
}