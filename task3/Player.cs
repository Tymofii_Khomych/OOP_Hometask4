﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class Player : IPlayable, IRecordable
    {
        public void Play()
        {
            Console.WriteLine("Player is gaming right now");
        }

        void IPlayable.Pause()
        {
            Console.WriteLine("Player is AFK");
        }

        void IPlayable.Stop()
        {
            Console.WriteLine("Player is not in game");
        }

        public void Record()
        {
            Console.WriteLine("Player is recording a game");
        }

        void IRecordable.Pause()
        {
            Console.WriteLine("Player paused a record");
        }

        void IRecordable.Stop()
        {
            Console.WriteLine("Player stopped recording");
        }
    }
}
